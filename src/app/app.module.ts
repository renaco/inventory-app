import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { 
  RouterModule,
  Routes,
  Router
} from '@angular/router';

import { UserModule } from './user/user.module';
import { AnalyticsModule } from './analytics/analytics.module';

import {
  APP_BASE_HREF,
  LocationStrategy,
  HashLocationStrategy
} from '@angular/common';

// import {
//   Http
// } from '@angular/Http';
import { HttpModule } from '@angular/http';


import {
  FormsModule,
  ReactiveFormsModule
}  from '@angular/forms';

import { AppComponent } from './app.component';
import { SidebarComponent } from './sidebar/sidebar.component'
import { SidebarItemComponent } from './sidebar/sidebar-item.component'
import { IntroComponent } from './intro/intro.component'

import { ExampleDef } from './example.model';
import { UserComponent } from './user/user.component';
import { AnalyticsComponent } from './analytics/analytics.component';
// import { SimpleHttpComponent } from './simple-http/simple-http.component';

import { YouTubeSearchComponent } from './you-tube-search/you-tube-search.component';
import { SearchResultComponent } from './you-tube-search/search-result/search-result.component';
import { SearchBoxComponent } from './you-tube-search/search-box/search-box.component';
import { youTubeSearchInjectables } from './you-tube-search/you-tube-search.injectable';

// for use in all aplication
export const examples: ExampleDef[] = [
  { label: 'Intro', name: 'Root', path: '', component: IntroComponent },
  { label: 'Injector', name: 'Injector', path: 'injector', component: UserComponent },
  { label: 'YouTube', name: 'YouTube', path: 'youtube', component: YouTubeSearchComponent }
  // { label: 'Factory', name: 'factory', path: 'factory', component: AnalyticsComponent },
  // { label: 'HTTP', name: 'http', path: 'http', component: SimpleHttpComponent }
];

// Routes

const routes: Routes = [
  { path: '', component: IntroComponent, pathMatch: 'full'},
  { path: 'injector', component: UserComponent, pathMatch: 'full'},
  { path: 'youtube', component: YouTubeSearchComponent, pathMatch: 'full'},
  // { path: 'http', component: SimpleHttpComponent, pathMatch: 'full'}
]

@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    SidebarItemComponent,
    IntroComponent,
    UserComponent,
    AnalyticsComponent,
    // SimpleHttpComponent,
    YouTubeSearchComponent,
    SearchResultComponent,
    SearchBoxComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    FormsModule,
    ReactiveFormsModule,
    UserModule,
    HttpModule,
    AnalyticsModule
  ],
  providers: [
    youTubeSearchInjectables,
    { provide: APP_BASE_HREF, useValue: '/' },
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    { provide: 'ExampleDefs', useValue: examples }
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
