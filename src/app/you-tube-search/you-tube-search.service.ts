import {
  Inject,
  Injectable
} from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/observable';

import { SearchResult } from './search-result.model';

export const YOUTUBE_API_KEY = 'AIzaSyBafzeucxPwm9zzVy2wpKQ5xDtKcGu9bq4';
export const YOUTUBE_API_URL = 'http://www.googleapis.com/youtube/v3/search';

@Injectable()
export class YouTubeSearchService {
  constructor(
    private http: Http,
    @Inject(YOUTUBE_API_KEY) private apiKey: string,
    @Inject(YOUTUBE_API_URL) private apiUrl: string
  ) {

  }

  search( query: string ): Observable<SearchResult[]> {
    const params: string = [
      `q=${query}`,
      `key=${this.apiKey}`,
      `part=snippet`,
      `type=video`,
      `maxResults=10`
    ].join('&');
    const queryUrl = `${this.apiUrl}?${params}`;

    return this.http.get(queryUrl)
      .map((response: Response) => {
        return (<any>response.json()).items.map( item => {
          console.log('item', item)
          return new SearchResult({
            id: item.id.videoId,
            title: item.snippet.title,
            description: item.snippet.description,
            thumbnailUrl: item.snippet.thumbnailUrl,
            videoUrl: item.snippet.videoUrl
          })
        })
      })
  }
}