export interface ExampleDef {
  label: string; // links
  name: string; // Nams routes
  path: string; // Routes
  component: any; // Component class
  dev?: boolean; // intermediate steps
}
