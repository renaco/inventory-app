import { Component, OnInit } from '@angular/core';
import {
  Http,
  Response,
  RequestOptions,
  Headers
} from '@angular/Http'

@Component({
  selector: 'app-simple-http',
  templateUrl: './simple-http.component.html',
  styleUrls: ['./simple-http.component.css']
})
export class SimpleHttpComponent implements OnInit {
  data: Object;
  loading: boolean;
  myUrl: string;

  constructor(
    private http: Http
  ) { 
    this.myUrl = 'http://jsonplaceholder.typicode.com/posts/1';
  }

  myRequest(): void {
    this.loading = true;
    this.http.request(this.myUrl)
      .subscribe((res: Response) => {
        console.log(res)
        this.loading = false;
      })
  }

  makePost(): void {
    this.loading = true;
    this.http.post(this.myUrl, 
    JSON.stringify({
      body: 'bar',
      title: 'foo',
      userId: 1
    }))
      .subscribe((res: Response) => {
        console.log(res)
        this.loading = false;
      })
  }

  makeDelete(): void {
    this.loading = true;
    this.http.delete(this.myUrl)
      .subscribe((res: Response) => {
        console.log(res)
        this.loading = false;
      })
  }

  makeHeaders(): void {
    const headers: Headers = new Headers();
    headers.append('X_API_TOKEN', 'baby-drive');

    const opts: RequestOptions = new RequestOptions();
    opts.headers = headers;

    this.http.get(this.myUrl, opts)
      .subscribe((res: Response) => {
        this.data = res.json();
        this.loading = false;
      })
  }

  ngOnInit(): void {
    // this.myRequest();
  }

}
