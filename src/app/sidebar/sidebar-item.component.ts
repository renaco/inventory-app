import {
  Component,
  Input,
  OnInit
 } from '@angular/core';

import { Location } from '@angular/common';
import {
  Router,
  ActivatedRoute
 } from '@angular/router';

 import { ExampleDef } from '../example.model';

@Component({
  selector: 'app-sidebar-item',
  templateUrl: './sidebar-item.component.html'
})

export class SidebarItemComponent {
  @Input('item') item: ExampleDef;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private location: Location
  ) {

  }
  ngOnInit(){
    // console.log(this)
  }
  isActive(): boolean {
    console.log(this.location.path())
    return `/${this.item.path}` === this.location.path()
  }
}