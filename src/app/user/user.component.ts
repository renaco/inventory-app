import { Component, OnInit, ReflectiveInjector } from '@angular/core';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  userName: string;

  constructor(
    private userService: UserService
  ) { 
    const injector: any = ReflectiveInjector.resolveAndCreate([UserService])
  }

  signIn(): void {
    this.userService.setUser({
      name: 'Sergio'
    })

    this.userService = this.userService.getUser().name;
  }

  ngOnInit() {
  }

}
